﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DisplayActionSheetTest
{
    public class App : Application
    {
        public App()
        {
            // The root page of your application
            Button button = new Button
            {
                Text = "Click to show an action sheet and then try to click outside the displayed UI to dismiss...",
            };
            button.Clicked += async (sender, args) =>
            {
                string[] buttons =
                {
                    "choice one",
                    "choice two",
                    "choice three",
                };
                string selected = await MainPage.DisplayActionSheet("title", cancel: null, destruction: null, buttons: buttons);
                Debug.WriteLine($"Selected : {selected}");
            };

            MainPage = new ContentPage
            {
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        button,
                    }
                }
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
