﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ButtonBorderRadius
{
    public class App : Application
    {
        public App()
        {
            Button button = new Button
            {
                BorderRadius = 50,
                BorderWidth = 4,
                BorderColor = Color.Red,
                Text = "    a\r\n    multi-line\r\n    rounded button      ",
                // InputTransparent = false, -- should be default for buttons
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            button.Clicked += (sender, args) =>
            {
                Debug.WriteLine("Clicked...");
            };

            // The root page of your application
            MainPage = new ContentPage
            {
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Center,
                    Children =
                    {
                        new Label
                        {
                            Text = "On UWP, the button below is rounded, but mouse-over highlighting is a full square not clipped to the borders.  This doesn't happen on Win8.1."
                        },
                        button,
                    }
                }
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
