﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using Xamarin.Forms;

namespace ListViewsPerfTest
{
    public class App : Application
    {
        public App ()
        {
            // The root page of your application
            
            Button button = new Button
            {
                Text = "Click me to go to ListView test page",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };
            Label label = new Label
            {
                Text = @"
Both tests should be done with Windows 8.1 target (Note that UWP also appears to show the same/similar problems).

Test #1 : Enter the ListView test page and witness that some cells do not correctly draw subsequent colors (R,G,B in that order repeating).  Sometimes a color is repeated or skipped.
    - You may need to return here and re-enter the ListView test page to trigger the behavior
    - Scrolling rapidly top-to-bottom appears more likely to trigger the erroneous behavior

Test #2 : Build in release mode and start Debug > Performance Profiler...  Choose 'Memory Usage' and start the diagnostics session.  On the ListView page, aggressively scroll the view top-to-bottom and watch the memory grow (despite bounded number of cells).  Witness what appears to be unbounded memory growth despite GC events during scrolling.
    - Returning to the home page and re-entering ListView test page once does not appear to clear the allocated memory
    - However returning to the home page and re-entering the ListView test page 'a few more times' does appear to clear a significant portion of the allocated memory, but does appear to clear *all* of it
",
                FontSize = 14,
            };

            StackLayout sl = new StackLayout();
            sl.Children.Add(button);
            sl.Children.Add(label);

            ContentPage initialContentPage = new ContentPage
            {
                Content = sl,
            };

            button.Clicked += async (object sender, EventArgs args) =>
            {
                await initialContentPage.Navigation.PushAsync(new ListViewTestPage());
            };

            MainPage = new NavigationPage(initialContentPage);
        }

        protected override void OnStart ()
        {
            // Handle when your app starts
        }

        protected override void OnSleep ()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume ()
        {
            // Handle when your app resumes
        }
    }

    public class ListViewTestPage : ContentPage
    {
        private ListViewBindingContextModel _bindingModel;
        private ListView _listView;

        IDictionary<string, string> ListDataSource { get; }

        public ListViewTestPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            StackLayout sl = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };

            // Construct ListView with Binding Context ...

            _bindingModel = new ListViewBindingContextModel();
            _listView = new ListView
            {
                HasUnevenRows = true,
                SeparatorColor = Color.Blue,
                SeparatorVisibility = SeparatorVisibility.None,
                BindingContext = _bindingModel,
            };

            _listView.ItemTemplate = new DataTemplate(() =>
            {
                var cell = new ListViewCell();
                return cell;
            });

            _listView.SetBinding(ListView.ItemsSourceProperty, "ListDataSource");
            sl.Children.Add(_listView);

            Button backButton = new Button
            {
                Text = "< Back",
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start,
            };
            backButton.Clicked += async (object sender, EventArgs args) =>
            {
                await Navigation.PopAsync();
            };
            sl.Children.Add(backButton);

            Content = sl;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            // _listView.ItemsSource = null; -- doesn't appear to affect much here
        }
    }

    public class ListViewBindingContextModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private const uint CELL_COUNT = 512;

        private IList<string> _listDataSource = null;
        public IList<string> ListDataSource
        {
            get
            {
                if (_listDataSource == null)
                {
                    lock (this)
                    {
                        if (_listDataSource == null)
                        {
                            _listDataSource = new List<string>();
                            for (uint i = 0; i < CELL_COUNT; i++)
                            {
                                string item = $"item #{i}";
                                _listDataSource.Add(item);
                            }
                        }
                    }
                }
                return _listDataSource;
            }
        }
    }

    public class ListViewCell : ViewCell
    {
        private static int rgb = -1;
        private static int lockRef = 0;

        public ListViewCell() : base()
        {
        }

        protected override void OnBindingContextChanged()
        {
            // "lock" ...
            int previousVal = Interlocked.CompareExchange(ref lockRef, value: 1, comparand: 0);
            if (previousVal != 0)
            {
                // should not happen unless this is called concurrently with another cell (not witnessed) ...
                Debugger.Break();
            }

            base.OnBindingContextChanged();
            object bc = BindingContext;

            string item = (string)BindingContext;

            Interlocked.MemoryBarrier();
            int colorVal = Interlocked.Increment(ref rgb); // atomic increment color value and pathologically insure other thread(s) see the new value
            Interlocked.MemoryBarrier();

            colorVal %= 3;
            Color color;
            if (colorVal == 0)
            {
                color = Color.Red;
            }
            else if (colorVal == 1)
            {
                color = Color.Green;
            }
            else
            {
                color = Color.Blue;
            }

            View = new Label
            {
                BackgroundColor = color,
                TextColor = Color.White,
                FontSize = 14,
                Text = item,
            };

            // "unlock"
            Interlocked.Exchange(ref lockRef, 0);
        }
    }
}
