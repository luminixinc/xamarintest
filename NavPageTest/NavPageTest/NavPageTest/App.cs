﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace NavPageTest
{
	public class App : Application
	{
		public App ()
		{
			// The root page of your application
			ContentPage page = new MyContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							HorizontalTextAlignment = TextAlignment.Center,
							Text = "Welcome to Xamarin Forms!"
						}
					}
				}
			};

            MainPage = new NavigationPage(page);
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}

    public class MyContentPage : ContentPage
    {
        public MyContentPage() : base()
        {
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
