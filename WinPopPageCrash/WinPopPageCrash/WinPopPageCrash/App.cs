﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;

#if __ANDROID__
using PCLStorage;
#else
using FileSystem = Windows.Storage.ApplicationData;
using IFolder = Windows.Storage.StorageFolder;
using IFile = Windows.Storage.StorageFile;
using Windows.Storage;
#endif


namespace WinPopPageCrash
{
    public class App : Application
    {
        public static App CurrentApp { get; private set; }

        public static IFolder CurrentAppFolder { get; private set; }

        public App()
        {
            MainPage = new NavigationPage(new FirstPage());
            CurrentApp = this;

#if __ANDROID__
            CurrentAppFolder = FileSystem.Current.LocalStorage;
#else
            CurrentAppFolder = FileSystem.Current.LocalFolder;
#endif
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }

    public class BasePage : ContentPage
    {
        protected virtual async Task BackButtonAction()
        {
            await PopPage(page: this, animated: true);
        }

        private async Task PopPage(Page page, bool animated)
        {
            await page.Navigation.PopAsync(animated);
        }

        protected async Task DoSomeAsyncStuff()
        {
            try
            {
                IFile fil = await App.CurrentAppFolder.CreateFileAsync("testfile", CreationCollisionOption.ReplaceExisting);
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Caught exception : {e}");
            }
        }

    }

    public class FirstPage : BasePage
    {
        protected override void OnAppearing()
        {
            base.OnAppearing();
            Button pushButton = new Button
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Text = "Click to Push page!"
            };
            pushButton.Clicked += async (sender, args) =>
            {
                ////Task.Run(async () =>
                {
                    await DoSomeAsyncStuff();

#if false
                    Action bbHandler = async () =>
                    {
                        await BackButtonAction();
                    };
                    SecondPage secondPage = new SecondPage(bbHandler);
#else
                    SecondPage secondPage = await GetSecondPageAsync();
#endif

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await App.CurrentApp.MainPage.Navigation.PushAsync(secondPage);
                    });
                };////);
            };

            Content = pushButton;
        }

        public static Task<SecondPage> GetSecondPageAsync()
        {
            return Task<SecondPage>.Run(() =>
            {
                var tcs = new TaskCompletionSource<SecondPage>();
                Device.BeginInvokeOnMainThread(() =>
                {
                    SecondPage secondPage = new SecondPage(null);
                    tcs.SetResult(secondPage);
                });

                return tcs.Task;
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }

    public class SecondPage : BasePage
    {

        public static double PageWidth { get; private set; }

        public Action BackButtonHandler { get; set; }

        private ListViewModel _viewModel;

        public SecondPage(Action bbHandler) : base()
        {
            BackButtonHandler = bbHandler;
            _viewModel = new ListViewModel();
        }

        protected override sealed void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            PageWidth = width;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await DoSomeAsyncStuff();

            BackButtonHandler = async () =>
            {
                await BackButtonAction();
            };





            ListView listView = new LUListView
            {
                HasUnevenRows = true,
                SeparatorColor = Color.Red,
                SeparatorVisibility = SeparatorVisibility.None,
                BindingContext = _viewModel
            };

            listView.ItemTemplate = new DataTemplate(() =>
            {
                //dataTemplateCounter++;
                //log.Info($"data template counter: {dataTemplateCounter}");
                var cell = new MyViewCell();
                cell.BackgroundColor = NextCellShouldBeLighter ? Color.Pink : Color.Lime;
                return cell;
            });

            listView.SetBinding(ListView.ItemsSourceProperty, "ListDataSource");
            //listView.ItemTapped += OnTapped;




            Label backLabel = new Label
            {
                Text = "go back",
                FontSize = 32,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };
            TapGestureRecognizer tapG0 = new TapGestureRecognizer();
            tapG0.Tapped += (sender, args) =>
            {
                ////Navigation.PushAsync(new ThirdPage(BackButtonHandler));
                BackButtonHandler();
            };
            backLabel.GestureRecognizers.Add(tapG0);

            Label pushLabel = new Label
            {
                Text = "go forward",
                FontSize = 32,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                VerticalOptions = LayoutOptions.Center,
            };
            TapGestureRecognizer tapG = new TapGestureRecognizer();
            tapG.Tapped += async (sender, args) =>
            {
                SecondPage secondPage = await FirstPage.GetSecondPageAsync();

                Navigation.PushAsync(new SecondPage(BackButtonHandler));
            };
            pushLabel.GestureRecognizers.Add(tapG);

            StackLayout sl0 = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    backLabel,
                    pushLabel,
                },
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };



            StackLayout sl = new StackLayout
            {
                Children =
                {
                    sl0,
                    listView,
                },
            };

            Content = sl;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        protected bool _nextCellShouldBeLighter;
        private bool NextCellShouldBeLighter
        {
            get
            {
                _nextCellShouldBeLighter = !_nextCellShouldBeLighter;
                return _nextCellShouldBeLighter;
            }
        }
    }


    public class LUListView : ListView
    {
    }



    public class ThirdPage : BasePage
    {

        public Action BackButtonHandler { get; set; }

        private ListViewModel _viewModel;

        public ThirdPage(Action bbHandler) : base()
        {
            BackButtonHandler = bbHandler;
            _viewModel = new ListViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await DoSomeAsyncStuff();

            BackButtonHandler = async () =>
            {
                await BackButtonAction();
            };





            ListView listView = new LUListView
            {
                HasUnevenRows = true,
                SeparatorColor = Color.Red,
                SeparatorVisibility = SeparatorVisibility.None,
                BindingContext = _viewModel
            };

            listView.ItemTemplate = new DataTemplate(() =>
            {
                //dataTemplateCounter++;
                //log.Info($"data template counter: {dataTemplateCounter}");
                var cell = new MyViewCell();
                cell.BackgroundColor = NextCellShouldBeLighter ? Color.Pink : Color.Lime;
                return cell;
            });

            listView.SetBinding(ListView.ItemsSourceProperty, "ListDataSource");
            //listView.ItemTapped += OnTapped;




            Label label = new Label
            {
                Text = "Tap to go Backward",
                FontSize = 32,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };


            TapGestureRecognizer tapG = new TapGestureRecognizer();
            tapG.Tapped += (sender, args) =>
            {
                ////Navigation.PushAsync(new ThirdPage());
                BackButtonHandler();
            };
            label.GestureRecognizers.Add(tapG);


            StackLayout sl = new StackLayout
            {
                Children =
                {
                    label,
                    listView,
                },
            };

            Content = sl;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        protected bool _nextCellShouldBeLighter;
        private bool NextCellShouldBeLighter
        {
            get
            {
                _nextCellShouldBeLighter = !_nextCellShouldBeLighter;
                return _nextCellShouldBeLighter;
            }
        }
    }









    public class MyViewCell : ViewCell
    {
        public virtual Color BackgroundColor { get; set; }
        public virtual Color SelectionColor { get; set; }
        public virtual Color MainTextColor { get; set; }
        public virtual Color LabelTextColor { get; set; }

        private BoxView _cell00Background;
        private Grid _cellGrid;
        protected Label _checkmarkLabel;

        public MyViewCell()
        {
            BackgroundColor = Color.Pink;
            MainTextColor = Color.Black;
            LabelTextColor = Color.Blue;
            _cell00Background = new Xamarin.Forms.BoxView();
            ContentView tempView = new Xamarin.Forms.ContentView
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Content = new Xamarin.Forms.Label
                {
                    Text = "placeholder",
                    FontSize = 20,
                    TextColor = Color.Black,
                },
            };
            View = tempView; // just set up the cell so that we dont crash if X.F calls the renderer before we call DrawLayout
            // set selection color based on background color
            double lightFactor = 1.1;  // increase by 10%
            Color mbg = BackgroundColor;
            SelectionColor = Color.FromRgb(mbg.R * lightFactor, mbg.G * lightFactor, mbg.B * lightFactor);
        }




        static long[] samples = new long[10000];
        static uint counter = 0;
        protected static double averageSamples()
        {
            long count = 0;
            uint samps = 0;
            for (uint i = 0; i < counter; i++)
            {
                long samp = samples[i];
                if (samp > 0)
                {
                    count += samp;
                    ++samps;
                }
            }
            if (samps == 0)
            {
                samps = 1;
            }
            return count / samps;
        }







        protected override void OnBindingContextChanged()
        {
            long millisBegin = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            //HACKishness
            if (false) {
                long millisPrevious = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                long millisWait = new Random().Next(1, 30);
                while (true)
                {
                    long millisNow = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    if (millisNow - millisPrevious > millisWait)
                    {
                        break;
                    }
                }
            }

            try
            {
                base.OnBindingContextChanged();

                //
                // Casting the BindingContext to the data that was expected and then use it to draw some dynamic views.
                //
                if (BindingContext != null)
                {
                    DrawLayout(new Dictionary<string, string>());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Caught exception in binding context : {ex}");
            }

            long millisEnd = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            samples[counter++] = millisEnd - millisBegin;
        }

        public virtual void DrawLayout(IDictionary<string, string> dict)
        {
            // table cell is a 2x2 grid: 
            // main content is at top left grid cell and takes the majority of the grid real estate.
            // the top right grid cell is reserved for an option checkmark
            // the 2nd row is reserved entirely for a line separator (Windows only)

            // create the grid
            _cellGrid = new Xamarin.Forms.Grid
            {
                RowDefinitions =
                    {
                        new RowDefinition
                        {
                            Height = new GridLength(1, GridUnitType.Star)
                        },
                        new RowDefinition
                        {
                            Height = new GridLength(1, GridUnitType.Absolute)
                        }
                    },
                ColumnDefinitions =
                    {
                        new ColumnDefinition
                        {
                            Width = new GridLength(1, GridUnitType.Star)
                        },
                        new ColumnDefinition
                        {
                            Width = new GridLength(1, GridUnitType.Auto)
                        }
                    },
                Padding = 8,
                BackgroundColor = BackgroundColor
            };

            // add the background to the content position (0,0)
            _cell00Background.Color = BackgroundColor;
            _cellGrid.Children.Add(_cell00Background, 0, 0);

            // For some reason windows is ignoring the MR Gestures tap on this cell, so we use a regular 
            // tap gesture recognizer.
#if false
//#if PLATFORM_WINDOWS
            TapGestureRecognizer gestureRecognizer = new TapGestureRecognizer();
            gestureRecognizer.Tapped += (s, e) =>
            {
                if (IsMultiSelect) { IsSelected = !IsSelected; }

                if (!ShouldBlockTapAction && TapAction != null) TapAction(this);
                else ShouldBlockTapAction = !ShouldBlockTapAction;
            };
            _cellGrid.GestureRecognizers.Add(gestureRecognizer);
#endif

            {
                _checkmarkLabel = new Xamarin.Forms.Label
                {
                    Text = "\u2714",
                    TextColor = MainTextColor,
                    FontSize = 18,
                    IsVisible = true,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    YAlign = TextAlignment.Center,
                };

                _cellGrid.Children.Add(_checkmarkLabel, 1, 0);
            }

            View = _cellGrid;

            View content = EvenlySpacedLabelGridRow(SecondPage.PageWidth, hasBorders: false);
            _cellGrid.Children.Add(content, 0, 0);
        }

        public static Layout EvenlySpacedLabelGridRow(double width, bool hasBorders, bool isHeader = false)
        {
            StackLayout content = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };
            /*if (hasBorders)
            {
                BoxView b = BorderLine(JustAShadeDarker(Setting.mainBackgroundColor), LayoutOptions.StartAndExpand);
                content.Children.Add(b);
            }*/

            double COLUMN_SPACING = 16.0;
            Grid grid = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition { Height = isHeader ? 30 : new GridLength(30, GridUnitType.Star) },
                },
                ColumnSpacing = COLUMN_SPACING,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };

            int MAX_COLUMNS = 10; // only show the first 10 columns
            int columnCount = 1;

            List<string> textvalues = new List<string> { "Foobar", "barfoo", "fubar", "rockstarz", "ninjaz" };

            if (new Random().Next(0, 10) == 0)
            {
                int idx = new Random().Next(0, 4);
                textvalues[idx] = "Rockstarz, Ninjaz, Guruz, Geniusez, 10-timez-engineerz, 'doze geekz, and other General Denizenz of Redmond";
            }

            int numberOfColumns = Math.Min(MAX_COLUMNS, textvalues.Count);
            double gridWidth = (width - numberOfColumns * COLUMN_SPACING) / numberOfColumns;

            foreach (string labelData in textvalues)
            {
                if (columnCount++ < MAX_COLUMNS)
                {
                    ColumnDefinition cd = new ColumnDefinition { Width = new GridLength(gridWidth) };
                    grid.ColumnDefinitions.Add(cd);
                }
            }

            int fromLeft = 0;
            foreach (string labelData in textvalues)
            {
                if (fromLeft < MAX_COLUMNS - 1)
                {
                    Label label = new Label
                    {
                        Text = labelData,
                        FontAttributes = isHeader ? FontAttributes.Bold : FontAttributes.None,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalOptions = LayoutOptions.Start,
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        LineBreakMode = isHeader ? LineBreakMode.TailTruncation : LineBreakMode.WordWrap,  // at least until we dynamically resize header height
                    };

                    grid.Children.Add(label, fromLeft++, 0);
                }
            }

            content.Children.Add(grid);
            /*if (hasBorders)
            {
                content.Children.Add(BorderLine(JustAShadeDarker(Setting.mainBackgroundColor), LayoutOptions.EndAndExpand));
            }*/

            return content;
        }

    }

    public class ListViewModel : INotifyPropertyChanged
    {
        public virtual event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected IDictionary<string, string> _listDataSource;
        public virtual IDictionary<string, string> ListDataSource
        {
            get
            {
                if (_listDataSource == null)
                {
                    const int capacity = 100;
                    _listDataSource = new Dictionary<string, string>(capacity);
                    for (int i = 0; i < capacity; i++)
                    {
                        _listDataSource[$"{i}"] = $"{i}";
                    }
                }
                return _listDataSource;
            }

            set
            {
                _listDataSource = value;
                OnPropertyChanged("ListDataSource");
            }
        }
    }
}
