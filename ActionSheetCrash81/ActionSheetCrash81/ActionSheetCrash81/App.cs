﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ActionSheetCrash81
{
	public class App : Application
	{
		public App ()
		{
			// The root page of your application
			MainPage = new ActionSheetCrash81ContentPage();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}

    public class ActionSheetCrash81ContentPage : ContentPage
    {
        public ActionSheetCrash81ContentPage()
        {
            Button displayActionSheetButton = new Button
            {
                Text = "tap to DisplayActionSheet",
                BackgroundColor = Color.Blue,
                Command = new Command(/*async*/ () =>
                {
                    List<string> labels = new List<string>();
                    labels.Add("Foo");
                    labels.Add("Bar");
                    labels.Add("Baz");

                    // TESTCASE:
                    // - Massively spam this button and the resulting ActionSheet on Win8.1
                    // - Notice Xamarin crashes possibly because https://github.com/xamarin/Xamarin.Forms/blob/release-2.3.1/Xamarin.Forms.Platform.WinRT/Platform.cs#L646
                    //   sets the _currentActionSheet to null and then likely receives a second tap which triggers NullReferenceException
                    // - CAVEAT : may need to "slow down" your test machine with a side-channel delay processes (e.g. : dd if=/dev/zero of=/dev/null )

                    /*string response = await*/
                    DisplayActionSheet("Try to double tap any of these", null, null, labels.ToArray());
#if false
                    // not necessary for a repro ...
                    if (response == null || response == "Cancel")
                    {
                        System.Diagnostics.Debug.WriteLine($"Nothing selected");
                        return;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine($"{response} selected");
                        return;
                    }
#endif
                })
            };
            Content = displayActionSheetButton;
        }
    }
}
